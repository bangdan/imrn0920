console.log('============= no 1 looping ==============')
console.log (' ')
console.log ('LOOPING PERTAMA')

var counter = 0
var genap = 2
while(counter < 20){
    counter += genap
    console.log(counter + ' - I Love Coding')
}
console.log ('LOOPING KEDUA')

var bomb = 22
var baru = 2
while(bomb > 2) {
    bomb -= baru
    console.log(bomb + ' - I will become a mobile developer')
}
console.log(' ')
console.log('============= no 2 looping ==============')
console.log(' ')

for(var angka = 1; angka < 21; angka++) {
    console.log(angka = 1 + ' - santai');
    console.log(angka = 2 + ' - berkualitas');
    console.log(angka = 3 + ' - I love coding');
    console.log(angka = 4 + ' - berkualitas');
    console.log(angka = 5 + ' - santai');
    console.log(angka = 6 + ' - berkualitas');
    console.log(angka = 7 + ' - santai');
    console.log(angka = 8 + ' - berkualitas');
    console.log(angka = 9 + ' - I love coding');
    console.log(angka = 10 + ' - berkualitas');
    console.log(angka = 11 + ' - santai');
    console.log(angka = 12 + ' - berkualitas');
    console.log(angka = 13 + ' - santai');
    console.log(angka = 14 + ' - berkualitas');
    console.log(angka = 15 + ' - I love coding');
    console.log(angka = 16 + ' - berkualitas');
    console.log(angka = 17 + ' - santai');
    console.log(angka = 18 + ' - berkualitas');
    console.log(angka = 19 + ' - santai');
    console.log(angka = 20 + ' - berkualitas')
  } 

console.log(' ')
console.log('============= no 3 looping ==============')
console.log(' ')

var string = '';
for(var angka = 1; angka < 5; angka++ ) {
    for(var deret = 1; deret < 9; deret++ ) {
        string += '#'
    }
    string +='\n'
}
console.log(string);

console.log(' ')
console.log('============= no 4 looping ==============')
console.log(' ')

var string ='';
for(var angka = 0; angka <7; angka++) {
    for(var deret = 0; deret <= angka; deret++) {
        string += '#';
    }
    string +='\n'
}
console.log(string)

console.log(' ')
console.log('============= no 5 looping ==============')
console.log(' ')

var string = '';
for(var angka = 1; angka < 9; angka++ ) {
    for(var deret = 1; deret < 9; deret++ ) {
       if(angka%2==0){
        if(deret%2==0){
        string += '#';
        } else
        {
            string += ' ';
        }
    }
    else 
    {
        if(deret%2==0){
            string += ' ';
            } else
            {
                string += '#';
            }
    }
    }
    string +='\n'
}
console.log(string);
