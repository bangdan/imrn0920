console.log('================ no 1 conditional =================')
console.log (" ")
var nama = ' '
var peran = ' '

if (nama == ' '){
    console.log ('nama harus diisi')
}
else if (nama && peran == ' '){
    console.log ('Halo: ' + nama + ', Pilih peranmu untuk memulai game!')
}
else if (nama== 'jane' && peran== 'penyihir'){
    console.log ("Selamat datang di Dunia Werewolf, Jane Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
else if (nama== 'jenita' && peran== 'guard'){
    console.log ("Selamat datang di Dunia Werewolf, Jenita Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf")
}
else if (nama== 'Junaedi' && peran== "Werewolf"){
    console.log ('Selamat datang di Dunia Werewolf, Junaedi Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
}

console.log (" ")
console.log (" ")

console.log('================ no 2 conditional =================')
console.log (" ")
var hari = 10
var bulan = 1
var tahun = 2019 

var bulanNama;

if(hari >= 1 && hari <= 31){
    if(bulan >= 1 && bulan <= 12) {
        if(tahun >= 1900 && tahun <= 2200){
            switch(bulan){
                case 1 :
                    bulanNama = 'januari';
                    break;
                case 2 :
                    bulanNama = 'februari';
                    break;
                case 3 :
                    bulanNama = 'maret';
                    break;
                case 4 :
                    bulanNama = 'april';
                    break;
                case 5 :
                    bulanNama = 'mei';
                    break;
                case 6 :
                    bulanNama = 'juni';
                    break;
                case 7 :
                    bulanNama = 'juli';
                    break;
                case 8 : 
                    bulanNama = 'agustus';
                    break;
                case 9 :
                    bulanNama = 'september';
                    break;
                case 10 :
                    bulanNama = 'oktober';
                    break;
                case 11 : 
                    bulanNama = 'november'
                    break;
                case 12 :
                    bulanNama = 'desember';
                    break;
                default :
                    break;

            }
            console.log(hari + ' ' + bulanNama + ' ' + tahun);
        }else{
            console.log('masukkan tahun diantara (1900 - 22000)');
        }
    }else{
        console.log('masukkan bulan diantara (1-12)');
    }
    }else{
        console.log('masukkan tanggal diantara (1-31)')
            
}


